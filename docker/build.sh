#!/bin/bash

set -e

IMAGE_NAME="ubuntu_zoe"
DOCKERFILE="Dockerfile"

# Build docker container
echo "Building Docker container: $IMAGE_NAME"
docker build \
    --rm \
    --network=host \
    --tag $IMAGE_NAME \
    --file $DOCKERFILE .
