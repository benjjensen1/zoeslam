#!/bin/bash

set -e

HOST_DIR=$PWD/..
DOCKER_DIR=/home/zoe/zoeslam
IMAGE_NAME="ubuntu_zoe"

# Start the container and mount host directory
docker run \
     -it \
     --rm \
     --net=host \
     --ipc=host \
     --privileged \
     --env="DISPLAY" \
     --volume=${HOST_DIR}:${DOCKER_DIR} \
     --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
     $IMAGE_NAME

#="$HOME/.Xauthority:/root/.Xauthority:rw" \
# EXPLANATION OF COMMANDS:
# -it:    start in interactive mode (i)  with a pseudo tty (t) 
# --rm:   close the docker container upon exit

# THIS COMMAND WORKS
#docker run \
#    -it --rm \
#    --privileged \
#    --volume ${HOST_DIR}:${DOCKER_DIR} \
#    --net=host \
#    --ipc=host \
#    $IMAGE_NAME



