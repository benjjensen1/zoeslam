# README #

This repository contains the code necessary to implement ORB-SLAM3 on the research rover Zoe, 
which is being implemented as a course project for "16-833: Robot Localization and Mapping." To clone the repository and all of its submodules, run

```git clone --recursive https://bitbucket.org/benjjensen1/zoeslam.git```

The submodules included in this repository are Pangolin and a forked version of ORB-SLAM3 that contains edits for integrating it with Zoe.

### Dependencies ###

This repository depends on:

- Ubuntu >= 18.04
- Eigen3 >= 3.1
- OpenCV >= 4.4
- ROS >= Melodic
- Pangolin (included as submodule)
    - Requires cmake >= 3.10 to build
- ORB-SLAM3 v1.0 (forked and included as submodule)
- Docker (optional) 
    - containerd.io: 1.4.3
    - CLI: 20.10.1
    - CE: 20.10.1

The repository includes a Dockerfile to build a container with the above specifications for Ubuntu, Eigen, OpenCV, and ROS.
The container will mount the host directory containing this repository to a directory within the container, exposing the code to the container,
and the container uses the host's network to allow access to an external ROS master (currently set up to search for a ROS master running on Zoe's
main computer). When initially starting the container, the code inside this repository (Pangolin, ORB-SLAM3, cv_bridge, and the ROS workspace with Zoe's ORB-SLAM3 nodes)
should be built within the container by running the `build_all.sh` script. This Docker setup was used to run the code on Zoe, which runs Ubuntu 16.04 as its host OS.

### Build Instructions ###

The `build_all.sh` script at the top level of this repository will build Pangolin, cv_bridge, ORB-SLAM3 (the library only, not the examples), and the ROS workspace,
assuming the other required dependencies have already been installed:  
```cd zoeslam```  
```./build_all.sh```  

## With Docker ##

To build the repository with Docker, the Docker container should first be built with `sudo ./build.sh` from within the docker subdirectory. The container image will require just under 3 GB
of space. Next, start the container with `sudo ./run.sh` (also in the docker subdirectory). The repository folder of the host device will be mounted to the ```/home/zoe/zoeslam``` directory 
in the container, meaning that any changes made to the local copy on the host device will immediately be visible in the Docker container.

After starting the container, run the regular build script to build the repository. Note that this build script will remove all associated build directories and build everything from scratch, 
so if re-building one specific component is required after the initial build inside a container, perform those steps separately. Build files that have been generated outside of the Docker 
container will not work inside the container.

The full work flow is shown below:  
```cd zoeslam```  
```sudo ./build.sh```  
```sudo ./run.sh```  
```[docker-container]# ./build_all.sh```  


## Launch Files ##

There are four launch files available for running ORB-SLAM3 on Zoe. Each of these launch files starts the image rotator node and runs the correct variant of ORB-SLAM3 with the appropriate 
parameter file. The options are:

- zoe_mono.launch
- zoe_mono_inertial.launch
- zoe_stereo.launch
- zoe_stereo_inertial.launch

The launch files can accept an argument for the display. By default, the display is set to ```false``` for all nodes; setting this to ```true``` will result in the Pangolin display appearing 
as ORB-SLAM3 runs.

These launch files will be available only after building and sourcing the cv_bridge workspace and ROS workspace. They are located in the ```launchers``` package.

## References ##
The original ORB-SLAM3 repository can be found on Github at https://github.com/UZ-SLAMLab/ORB_SLAM3; a forked copy of 
version 1.0 of ORB-SLAM3 has been included as a submodule for this project and contains minor edits. The corresponding publication can be found on arXiv at https://arxiv.org/abs/2007.11898.

The Pangolin submodule included within this repository can be found at https://github.com/stevenlovegrove/Pangolin.

A version of cv_bridge for ROS Melodic that is built with OpenCV 4.4 is included in this repository in the `cv_bridge` directory; the original code for this subfolder was taken 
from https://github.com/ros-perception/vision_opencv/tree/melodic/cv_bridge.

The Sophus to ROS conversions were taken from an old ```sophus_ros_conversions``` library for ROS Indigo: http://docs.ros.org/en/indigo/api/sophus_ros_conversions/html/namespacemembers.html. A few 
edits were made to update this library. The code for this library can be found in the ```workspace/src/zoe_orbslam/external/sophus_ros_conversions``` subdirectory.

## Example (Monocular) ##

Connect to 'zoe' wifi.  
**Start roscore on Zoe Main**  
```ssh zoe@1.0.0.3 (or zoe@zoe-main)```  
```roscore```  

**Set up rosbag (Zoe state)**  
```ssh zoe@1.0.0.4 (or zoe@zoe-state)```  
```cd zoeslam/datasets```  
```rosbag play --pause split_0001.bag```  

**Run image rectification (Zoe state)**  
```ssh zoe@:1.0.0.4``` (or zoe@zoe-state)  
```cd workspace/src/pointgrey_camera_driver/pointgrey_camera_driver/launch```     

Using the live feed from Zoe's cameras:   
```roslaunch nav_rect.launch```      

Using recorded images:   
```roslaunch nav_rect_no_driver.launch```   

**Build and run ORB_SLAM3 (Docker, Zoe state)**  
```ssh -X zoe@1.0.0.4 (or zoe@zoe_state)``` (The `-X` is needed for Pangolin)  
```cd zoeslam/docker```    
```sudo ./run.sh``` (enter Docker)  
```./build_all.sh``` (repository build)    
```cd cv_bridge``` 
```source devel/setup.bash``` (source cv_bridge)
```cd ../workspace```   
```source devel/setup.bash```   (source ROS workspace)
```roslaunch launchers zoe_mono.launch```  (run monocular ORB-SLAM3 node)
(Unpause the ros bag with `space`)

When the rosbag is done, use CTRL-C to quit out of orbslam, which will trigger it to save the results to a .txt file

