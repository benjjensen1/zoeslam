#!/bin/bash

# Pangolin
echo "Building Pangolin"
cd Pangolin && ./scripts/install_prerequisites.sh required
rm -rf build && mkdir build
cd build
cmake ..
cmake --build .
cd ../..

# Buid cv_bridge to use with ROS nodes
echo "Building cv_bridge"
cd cv_bridge && rm -rf build && rm -rf devel
catkin_make install
source devel/setup.bash
echo "export ROS_PACKAGE_PATH=/home/zoe/zoeslam/orb_slam3/Examples_old/ROS:$ROS_PACKAGE_PATH" >> ~/.bashrc
cd ..

# ORB-SLAM3
echo "Building ORB-SLAM3"
cd orb_slam3
rm -rf Thirdparty/DBoW2/build
rm -rf Thirdparty/g2o/build
rm -rf Thirdparty/Sophus/build
rm -rf build && mkdir build
chmod +x build.sh && ./build.sh

# ROS build for ORB-SLAM3
# echo "Building ORB-SLAM3 for ROS"
# rm -rf Examples_old/ROS/ORB_SLAM3/build
# sudo rosdep init && rosdep update
# chmod +x build_ros.sh && ./build_ros.sh
cd ..

# catkin workspace
echo "Building catkin workspace"
cd workspace && rm -rf build && rm -rf devel
catkin_make
