#!/usr/bin/env python

import rospy 
from sensor_msgs.msg import Image 
import numpy as np 
import ros_numpy 

import argparse

class ImageRotator:
    """
          Creates a ROS Node that reads in an image, rotates it 180*, 
        and publishes the rotated version (using ROS numpy, not cvBridge)
    """

    image_left  = None 
    image_right = None


    # image_rect vs image_raw 
    def __init__(self, args):
        """ 
              Initializes the ROS node and runs the image rotation
            function until ROS is shutdown  
        """

        rospy.init_node('flipped_image', anonymous = True)

        self.image_pub_left  = rospy.Publisher(args.pub_topic_left, Image, queue_size = 10) # "/nav_cameras/left/flipped_image", Image, queue_size = 10)
        self.image_sub_left  = rospy.Subscriber(args.sub_topic_right, Image, self.callback_left)

        if not args.mono:
            self.image_pub_right = rospy.Publisher(args.pub_topic_right, Image, queue_size = 10) # "/nav_cameras/right/flipped_image",  Image, queue_size = 10)
            self.image_sub_right = rospy.Subscriber(args.sub_topic_right, Image, self.callback_right)

        while not rospy.is_shutdown():
            if self.image_left is not None:
                if not args.dont_flip:
                    rotatedImage = np.rot90(np.rot90(self.image_left))   # cv2.rotate(self.image_left, cv2.cv2.ROTATE_180)
                    rotatedImageMsg = ros_numpy.msgify(Image, rotatedImage, encoding='mono8')               # self.cvBridge.cv2_to_imgmsg(rotatedImage)
                    self.image_pub_left.publish(rotatedImageMsg)
                else:
                    self.image_pub_left.publish(self.image_left)

            if not (args.mono) and (self.image_right is not None):
                if not args.dont_flip:
                    rotatedImage = np.rot90(np.rot90(self.image_right))  # cv2.rotate(self.image_right, cv2.cv2.ROTATE_180)
                    rotatedImageMsg = ros_numpy.msgify(Image, rotatedImage, encoding='mono8')               #self.cvBridge.cv2_to_imgmsg(rotatedImage)
                    self.image_pub_right.publish(rotatedImageMsg)
                else:
                    self.image_pub_right.publish(self.image_left)


    def callback_left(self, msg):
        """ Called when '/nav_cameras/left/image_raw' is updated. Updates self.image_left """

        rospy.loginfo("Left Image received...")
        # self.image_left = self.cvBridge.imgmsg_to_cv2(msg)
        self.image_left = ros_numpy.numpify(msg)

    def callback_right(self, msg):
        """ Called when '/nav_cameras/right/image_raw' is updated. Updates self.image_right """

        rospy.loginfo("Right Image received...")
        # self.image_right = self.cvBridge.imgmsg_to_cv2(msg)
        self.image_right = ros_numpy.numpify(msg)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--mono', action = "store_true")
    parser.add_argument('--sub_topic_left',
                         help = "Topic name for left camera",
                         default = "/nav_cameras/left/image_raw")
    parser.add_argument('--sub_topic_right',
                         help = "Topic name for right camera",
                         default = "/var_cameras/right/image_raw")
    parser.add_argument('--pub_topic_left',
                         help = "Topic name for left camera",
                         default = "/camera/left/image_raw")
    parser.add_argument('--pub_topic_right',
                         help = "Topic name for right camera",
                         default = "/camera/right/image_raw")
    parser.add_argument('--dont_flip', action = "store_false")

    args = parser.parse_args()

    try:
        ImageRotator(args)
    except rospy.ROSInterruptException:
        pass
