#!/usr/bin/env python

import rospy 
from sensor_msgs.msg import Image 
import numpy as np 
import ros_numpy 

class ImageRotator:
    """
          Creates a ROS Node that reads in an image, rotates it 180*, 
        and publishes the rotated version (using ROS numpy, not cvBridge)
    """

    image_left  = None 
    image_right = None
    left_header = None 
    right_header = None

 
    def __init__(self):
        """ 
              Initializes the ROS node and runs the image rotation
            function until ROS is shutdown  
        """

        rospy.init_node('image_rotator', anonymous = True)
        
        # NOTE that this publishes to "image_raw" in order to match the expected input to ORB SLAM3
        self.image_pub_left  = rospy.Publisher("/camera/left/image_raw", Image, queue_size = 10) 
        self.image_sub_left  = rospy.Subscriber("/nav_cameras/left/image_rect", Image, self.callback_left)

        self.image_pub_right = rospy.Publisher("/camera/right/image_raw", Image, queue_size = 10) 
        self.image_sub_right = rospy.Subscriber("/nav_cameras/right/image_rect", Image, self.callback_right)

        while not rospy.is_shutdown():
            rospy.spin()

    def callback_left(self, msg):
        """ Called when '/nav_cameras/left/image_raw' is updated. Updates self.image_left """
         
        rospy.loginfo("Left Image received...")
        #self.image_pub_left.publish(msg)
        left_header = msg.header
        left_image  = ros_numpy.numpify(msg)
        rotatedImage = np.rot90(np.rot90(left_image))
        rotatedImageMsg = ros_numpy.msgify(Image, rotatedImage, encoding='mono8')
        rotatedImageMsg.header = left_header
        self.image_pub_left.publish(rotatedImageMsg)
 
    def callback_right(self, msg):
        """ Called when '/nav_cameras/right/image_raw' is updated. Updates self.image_right """
 
        rospy.loginfo("Right Image received...")
        #self.image_pub_right.publish(msg)
        right_header = msg.header
        right_image  = ros_numpy.numpify(msg)
        rotatedImage = np.rot90(np.rot90(right_image))
        rotatedImageMsg = ros_numpy.msgify(Image, rotatedImage, encoding='mono8')      
        rotatedImageMsg.header = right_header
        self.image_pub_right.publish(rotatedImageMsg)


if __name__ == '__main__':
    try:
        ImageRotator()
    except rospy.ROSInterruptException:
        pass

