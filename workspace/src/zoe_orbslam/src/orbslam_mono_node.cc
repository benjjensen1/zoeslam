/**
* This file is part of ORB-SLAM3
*
* Copyright (C) 2017-2021 Carlos Campos, Richard Elvira, Juan J. Gómez Rodríguez, José M.M. Montiel and Juan D. Tardós, University of Zaragoza.
* Copyright (C) 2014-2016 Raúl Mur-Artal, José M.M. Montiel and Juan D. Tardós, University of Zaragoza.
*
* ORB-SLAM3 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with ORB-SLAM3.
* If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>

#include <opencv2/core/core.hpp>

#include "System.h"

#include "sophus_ros_conversions.hpp"
#include <nav_msgs/Odometry.h>

using namespace std;

class ImageGrabber
{
public:
    ImageGrabber(ORB_SLAM3::System* pSLAM, ros::NodeHandle* nh):
      mpSLAM(pSLAM),
      nh(nh),
      odom_counter(0){}

    void GrabImage(const sensor_msgs::ImageConstPtr& msg);

    ORB_SLAM3::System* mpSLAM;

    ros::NodeHandle* nh;
    ros::Publisher odom_pub = nh->advertise<nav_msgs::Odometry>("/odom", 1);
    uint64 odom_counter;
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Mono");
    ros::NodeHandle n("orbslam_mono");
    ros::start();

    if(argc < 4 || argc > 6)
    {
        cerr << endl << "Usage: rosrun zoe_orbslam orbslam_mono_node path_to_vocabulary path_to_settings image_topic [display]" << endl;
        ros::shutdown();
        return 1;
    }

    // Create SLAM system. It initializes all system threads and gets ready to process frames.
    bool display = false;
    if (argc == 5 && argv[4]) display = true;
    ORB_SLAM3::System SLAM(argv[1], argv[2], ORB_SLAM3::System::MONOCULAR, display);

    ImageGrabber igb(&SLAM, &n);

    // ros::Subscriber sub = n.subscribe("/camera/image_raw", 1, &ImageGrabber::GrabImage,&igb);
    ros::Subscriber sub = n.subscribe(argv[3], 1, &ImageGrabber::GrabImage, &igb);

    ros::spin();

    // Stop all threads
    SLAM.Shutdown();

    // Save camera trajectory
    // SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory_Mono.txt");
    // SLAM.SaveTrajectoryTUM("FrameTrajectory_Mono.txt");
    SLAM.SaveKeyFrameTrajectoryTUMAllMaps("KeyFrameTrajectory_MonoAll.txt");

    ros::shutdown();

    return 0;
}

void ImageGrabber::GrabImage(const sensor_msgs::ImageConstPtr& msg)
{
    // Copy the ros image message to cv::Mat.
    cv_bridge::CvImageConstPtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvShare(msg);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    Sophus::SE3f Tbw = mpSLAM->TrackMonocular(cv_ptr->image,cv_ptr->header.stamp.toSec());

    // Convert ORB-SLAM output to odometry and publish it
    geometry_msgs::Pose Pbw = sophus_ros_conversions::sophusToPoseMsg(Tbw);  
    nav_msgs::Odometry odom;
    odom.header.seq = odom_counter++;
    odom.header.stamp = ros::Time::now();
    odom.header.frame_id = "/odom";
    odom.pose.pose = Pbw;
    odom_pub.publish(odom);

}
