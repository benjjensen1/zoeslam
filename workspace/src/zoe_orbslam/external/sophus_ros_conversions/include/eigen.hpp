#ifndef sophus_ros_conversions_EIGEN_HPP_
#define sophus_ros_conversions_EIGEN_HPP_

#include <Eigen/Geometry>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>

namespace sophus_ros_conversions {

    void pointMsgToEigen(const geometry_msgs::Point &m, Eigen::Vector3f &e);
    void quaternionMsgToEigen(const geometry_msgs::Quaternion &m, Eigen::Quaternionf &e);
    Eigen::Quaternionf quaternionMsgToEigen(const geometry_msgs::Quaternion &m);
    
    geometry_msgs::Point eigenToPointMsg(Eigen::Vector3f &e);
    geometry_msgs::Quaternion eigenToQuaternionMsg(Eigen::Quaternionf &e);

}

# endif /* sophus_ros_conversions_EIGEN_HPP_ */

