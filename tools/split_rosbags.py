# Date: 3-30-2022
# Author: Margaret Hansen
# Purpose: Split Zoe rosbags into multiple

import argparse
import rosbag

# Function that splits a rosbag into n pieces
def split_rosbag(file, n):
    
    # Load file
    bagfile = rosbag.Bag(file)

    # Messages per bag
    msgs = bagfile.get_message_count()
    msgs_per_bag = int(round(float(msgs) / float(n)))

    # Loop through file and write n msgs per bag
    bag_ind = 0
    msg_ind = 0
    outbag = rosbag.Bag("split_%04d.bag" % bag_ind, 'w')
    for t, m, ts in bagfile.read_messages():
        msg_ind += 1
        if msg_ind % msgs_per_bag == 0:
            outbag.close()
            bag_ind += 1
            outbag = rosbag.Bag("split_%04d.bag" % bag_ind, 'w')
        outbag.write(t, m, ts)
    outbag.close()



if __name__ == "__main__":

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", help="rosbag to split", type=str)
    parser.add_argument("-n", help="number of bags to generate", type=int)
    args = parser.parse_args()

    # Call main function
    split_rosbag(args.file, args.n)
