# @date: 07-30-2021
# @author MAH
# @purpose Extract rover position and heading from bag file
# @arg --day Day for which command logs should be parsed
# @arg --data-path Path to command logs; defaults to /home/zoe/zoe-main-data/internal_data/command_logs
# @arg --out-path Path to store gps data to; defaults to /home/zoe/zoe-main-data/internal_data/gps_logs
# @arg --gps-prefix Prefix to add to output file; defaults to empty string

import rosbag
import rospy
from sensor_msgs.msg import NavSatFix
from nav_msgs.msg import Odometry
import pandas as pd
import numpy as np
import os
import argparse
import datetime
from tf import transformations as tf

def main():

    # Param: day to pull records for
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--day',
        help="Day to parse files for",
        type=str,
        default='2021-11-01'
    )
    parser.add_argument(
        '--data-path',
        help="Path to command logs",
        type=str,
        default='/home/zoe/zoe-main-data/internal_data/command_logs'
    )
    
    parser.add_argument(
        '--out-path',
        help="Path to store GPS logs to",
        type=str,
        default='/home/zoe/zoe-main-data/internal_data/gps_logs'
    )
    
    parser.add_argument(
        '--gps-prefix',
        help="Prefix to add to output file",
        type=str,
        default=''
    )

    args = parser.parse_args()
    print(args)

    # Save arguments in local variables
    day = args.day
    data_path = args.data_path
    out_file = args.out_path+'/'+args.gps_prefix+day+'.csv'

    # Get files in data directory
    files = os.listdir(data_path)

    # Filter to specified day
    files_to_read = [f for f in files if f.find(day) != -1]
    files_to_read.sort()
    # print(files_to_read)
    print("Reading "+str(len(files_to_read))+" files from "+data_path)

    # Topics
    gps_topic = '/vectornav/GPS'
    heading_topic = '/vectornav/EcefOdom'
    topic_list = [gps_topic, heading_topic]
    print(topic_list)

    # Counters
    c_gps = 0
    c_heading = 0

    # Write header to files
    gps_cols = ['time','latitude','longitude','altitude']
    heading_cols = ['time','heading']
    gps_df = pd.DataFrame(columns=gps_cols)
    heading_df = pd.DataFrame(columns=heading_cols)

    # Time values for comparison
    t_gps = 0
    t_heading = 0

    # Move to directory of bag files
    os.chdir(data_path)

    # Loop through bag files
    for i in range(len(files_to_read)):

        # Bag info
        print("Bag "+str(i+1)+" of "+str(len(files_to_read))+": "+files_to_read[i])
        bag = rosbag.Bag(files_to_read[i])

        # Loop through messages and save to appropriate data frame
        for tp, msg, t in bag.read_messages(topics=topic_list):
            
            # Odom
            if tp == gps_topic and t.secs > t_gps:
                
                lat = msg.latitude
                long = msg.longitude
                alt = msg.altitude
                new_gps_df = pd.DataFrame({'time': datetime.datetime.fromtimestamp(t.secs),
                                           'latitude': lat,
                                           'longitude': long,
                                           'altitude': alt},
                                           columns=gps_cols, index=[0])
                gps_df = gps_df.append(new_gps_df)
                c_gps+=1
                t_gps = t.secs
                # print("GPS: ")
                # print(c_gps)
                # print(gps_df.shape)
                # print("\n")

            # Heading
            elif tp == heading_topic and t.secs > t_heading:

                heading = msg.pose.pose.orientation
                yaw = tf.euler_from_quaternion([heading.x, heading.y, heading.z, heading.w])[2]*(180/np.pi)
                new_heading_df = pd.DataFrame({'time': datetime.datetime.fromtimestamp(t.secs), 'heading': yaw}, columns=heading_cols, index=[0])
                heading_df = heading_df.append(new_heading_df)
                c_heading+=1
                t_heading = t.secs
                # print("Heading: ")
                # print(c_heading)
                # print(heading_df.shape)
                # print("\n")

            else:
                continue

        print(gps_df.shape)
        print(heading_df.shape)

    # Parse the data into a better format
    print("GPS messages received: "+str(c_gps))
    print("Heading messages received: "+str(c_heading))
    print("Done reading bags, now parsing data")
    # print(gps_df.shape)
    # print(heading_df.shape)

    # Merge into one dataset and save
    merged_df = gps_df.merge(heading_df, on="time")

    # Save to file
    merged_df.to_csv(out_file, mode='a', header=True, index=False)


if __name__ == "__main__":
    main()

# EOF
