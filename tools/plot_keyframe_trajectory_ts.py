import sys
import numpy as np
import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial.transform import Rotation

filename = sys.argv[1]
ts_filename = sys.argv[2]
try:
    ekf_filename = sys.argv[3]
except:
    ekf_filename = None

# Read the files
with open(filename) as f:
    lines = f.readlines()

with open(ts_filename) as tsfile:
    tslines = tsfile.readlines()

if ekf_filename != None:
    with open(ekf_filename) as ekffile:
        ekflines = ekffile.readlines()

##################### KEYFRAME TRAJECTORY #####################

# Initialize arrays to store key frame trajectory
n = len(lines)
timestamps = np.zeros(n)
positions = np.zeros((n, 3))
positions_orig = np.zeros((n, 3))
quaternions = np.zeros((n, 4))

# Rotation for accounting for the pitch of the cameras
# and starting roll (estimated)
theta = np.radians(90-27.5)
c, s = np.cos(theta), np.sin(theta)
rotation1 = np.array([
    [1,  0,  0],
    [0,  c, -s],
    [0,  s,  c]
    ])
theta = np.radians(0)
c, s = np.cos(theta), np.sin(theta)
rotation2 = np.array([[c,  0,  s], [0,  1,  0], [-s,  0,  c]])
rotation = np.matmul(rotation2, rotation1)

# Store the keyframe trajectory and account for pitch of cameras
for i in range(0, n):
     line_arr = np.asarray(lines[i].split(), dtype=float)
     timestamps[i] = line_arr[0]
     positions_orig[i] = -1 * line_arr[1:4]
     positions[i] = np.matmul(rotation, positions_orig[i])
     quaternions[i] = line_arr[4:]

##################### TOTAL STATION TRAJECTORY #####################

# Initialize arrays to store TS trajectory
n = len(tslines)
positions_gt_orig = np.zeros((n, 3))
positions_gt = np.zeros((n, 3))

# Store the TS positions
for i in range(1, n+1):
    line = tslines[i-1].split(',')
    positions_gt_orig[n-i,0] = float(line[2]) - 2000 # easting
    positions_gt_orig[n-i,1] = float(line[1]) - 1000 # northing
    positions_gt_orig[n-i,2] = -1 * (float(line[3]) - 3000) # elevation

# Transform TS data to be centered at (0,0)
first_pose = np.array([positions_gt_orig[0,0], positions_gt_orig[0,1], positions_gt_orig[0,2]])
for i in range(0, n):
    # Center at zero
    positions_gt[i] = np.subtract(positions_gt_orig[i], first_pose)

##################### EKF TRAJECTORY #####################

# Initialize arrays to store EKF trajectory
n = len(ekflines) - 1
positions_ekf_orig = np.zeros((n, 3))
positions_ekf = np.zeros((n, 3))
quaternions_ekf = np.zeros((n, 4))

# Store the EKF positions and heading
for i in range(0, n):
     line = ekflines[i+1].split(',')
     positions_ekf_orig[i] = line[1:4]
     quaternions_ekf[i] = line[4:8]

# Transform EKF data to meters centered at (0,0)
first_pose = np.array([positions_ekf_orig[0,0], positions_ekf_orig[0,1], positions_ekf_orig[0,2]])
first_quat = quaternions_ekf[0]
first_quat[0:3] = -1 * quaternions_ekf[0, 0:3]
rotation = Rotation.from_quat(first_quat).as_dcm()
c, s = np.cos(np.pi/2), np.sin(np.pi/2)
y_forward_rotation = np.array([[c, -s,  0], [s,  c,  0], [0,  0,  1]])
c, s = np.cos(-np.pi), np.sin(-np.pi)
imu_rotation = np.array([[c,  0,  s], [0,  1,  0], [-s,  0,  c]])
for i in range(0, n):
    # Center at zero
    positions_ekf[i] = np.subtract(positions_ekf_orig[i], first_pose)
    # Rotate to account for original quaternion
    positions_ekf[i] = np.matmul(rotation, positions_ekf[i])
    # Rotate to account for y-forward heading
    positions_ekf[i] = np.matmul(y_forward_rotation, positions_ekf[i])
    # Rotate to account for difference in IMU frame?
    positions_ekf[i,0] *= -1
    # positions_ekf[i] = np.matmul(imu_rotation, positions_ekf[i])

# Calculate the total distance travelled according to EKF data
dist_ekf = 0
dist_ekf_2d = 0
for i in range(1, len(positions_ekf)):
    diff = np.array([positions_ekf[i-1,0] - positions_ekf[i,0],
                     positions_ekf[i-1,1] - positions_ekf[i,1],
                     positions_ekf[i-1,2] - positions_ekf[i,2]])
    dist_ekf += np.linalg.norm(diff)
    dist_ekf_2d += np.linalg.norm(diff[0:2])

##################### SCALE AND ROTATE TRAJECTORY #####################

# Calculate the total distance travelled according to TS data
dist_gt = 0
dist_gt_2d = 0
for i in range(1, len(positions_gt)):
    diff = np.array([positions_gt[i-1,0] - positions_gt[i,0],
                     positions_gt[i-1,1] - positions_gt[i,1],
                     positions_gt[i-1,2] - positions_gt[i,2]])
    dist_gt += np.linalg.norm(diff)
    dist_gt_2d += np.linalg.norm(diff[0:2])

# Calculate the total distance travelled according to keyframe trajectory
dist_kf = 0
for i in range(1, len(positions)):
    diff = np.array([positions[i-1,0] - positions[i,0],
                     positions[i-1,1] - positions[i,1],
                     positions[i-1,2] - positions[i,2]])
    dist_kf += np.linalg.norm(diff)
    
# Account for scale based on TS data
positions = positions * (dist_gt/dist_kf)

# Find the point 1 m from the origin on TS data
max_dist = 1.0
max_idx_gt = 0
for i in range(0, len(positions_gt)):
    dist = np.sqrt((positions_gt[i,0])**2 + (positions_gt[i,1])**2)
    if dist >= max_dist:
        # max_dist = dist
        max_idx_gt = i
        break

# Find the point 1 m from the origin on keyframe trajectory
max_dist = 1.0
max_idx_kf = 0
for i in range(0, len(positions)):
    dist = np.sqrt((positions[i,0])**2 + (positions[i,1])**2)
    if dist >= max_dist:
        # max_dist = dist
        max_idx_kf = i
        break


# Rotation from y axis to TS data
vector = np.array([positions_gt[max_idx_gt,0] - positions_gt[0,0],
                   positions_gt[max_idx_gt,1] - positions_gt[0,1]])
unit_vector = vector / np.linalg.norm(vector)
yaxis = np.array([0.0, 1.0])
dot_product = np.dot(unit_vector, yaxis)
if vector[0] < 0:
    theta_ts = 2 * np.pi - np.arccos(dot_product)
else:
    theta_ts = np.arccos(dot_product)

# Rotation from y axis to keyframe trajectory
vector = np.array([positions[max_idx_kf,0] - positions[0,0],
                   positions[max_idx_kf,1] - positions[0,1]])
unit_vector = vector / np.linalg.norm(vector)
yaxis = np.array([0.0, 1.0])
dot_product = np.dot(unit_vector, yaxis)
if vector[0] < 0:
    theta = theta_ts - (2 * np.pi - np.arccos(dot_product))
else:
    theta = theta_ts - np.arccos(dot_product)

c, s = np.cos(theta), np.sin(theta)
rotation = np.array([
    [c, -s,  0],
    [s,  c,  0],
    [0,  0,  1]
    ])

# Transform TS data to match the keyframe data
for i in range(0, len(positions_gt)):
    # Rotate to account for heading
    positions_gt[i] = np.matmul(rotation, positions_gt[i])

##################### RESCALING AND ERROR CALCULATION #####################

# Calculate the error in positions
error = np.sqrt((positions_gt[-1,0]-positions[-1,0])**2
              + (positions_gt[-1,1]-positions[-1,1])**2
              + (positions_gt[-1,2]-positions[-1,2])**2)

# Calculate the error in positions for EKF trajectory
error_ekf = np.sqrt((positions_gt[-1,0]-positions_ekf[-1,0])**2
                  + (positions_gt[-1,1]-positions_ekf[-1,1])**2
                  + (positions_gt[-1,2]-positions_ekf[-1,2])**2)

# z error
z_error = np.abs(positions_gt[-1,2] - positions[-1,2])
z_error_ekf = np.abs(positions_gt[-1,2] - positions_ekf[-1,2])

print('Absolute Error ORBSLAM: {:.5} m'.format(error))
print(' Percent Error ORBSLAM: {:.3%}'.format(error/dist_gt))
print(' Z-drift Error ORBSLAM: {:.5} m').format(z_error)

print('Absolute Error EKF: {:.5} m'.format(error_ekf))
print(' Percent Error EKF: {:.3%}'.format(error_ekf/dist_gt))
print(' Z-drift Error EKF: {:.5} m').format(z_error_ekf)
print('Absolute Distance Error EKF: {:.5} m'.format(np.abs(dist_ekf - dist_gt)))
print(' Percent Distance Error EKF: {:.3%}'.format(np.abs(dist_ekf - dist_gt)/dist_gt))
print('Absolute 2D Distance Error EKF: {:.5} m'.format(np.abs(dist_ekf_2d - dist_gt_2d)))
print(' Percent 2D Distance Error EKF: {:.3%}'.format(np.abs(dist_ekf_2d - dist_gt_2d)/dist_gt_2d))

##################### PLOTS #####################

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot3D(positions_gt[:, 0], positions_gt[:, 1], positions_gt[:, 2])
ax.plot3D(positions_ekf[:, 0], positions_ekf[:, 1], positions_ekf[:, 2])
ax.plot3D(positions[:, 0], positions[:, 1], positions[:, 2])
ax.axis('equal')
# ax.legend(['TS', 'EKF', 'ORB-SLAM3'])
ax.set_title('Total Station Data vs. Monocular ORB-SLAM3 vs. Extended Kalman Filter')
ax.set_xlabel('Left-Right Position (m)')
ax.set_ylabel('Forward Position (m)')
ax.set_zlabel('Elevation (m)')

fig = plt.figure()
ax = plt.axes()
ax.grid()
ax.scatter(positions_gt[:, 0], positions_gt[:, 1], marker='.')
ax.scatter(positions_ekf[:, 0], positions_ekf[:, 1], marker='.')
ax.scatter(positions[:, 0], positions[:,1], marker='.')
ax.axis('equal')
ax.legend(['TS', 'EKF', 'ORB-SLAM3'])
ax.set_title('Total Station Data vs. Monocular ORB-SLAM3 vs. Extended Kalman Filter')
ax.set_xlabel('Left-Right Position (m)')
ax.set_ylabel('Forward Position (m)')

plt.show()
