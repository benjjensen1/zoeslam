import sys
import numpy as np
import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial.transform import Rotation

mono_filename = sys.argv[1]
gps_filename = sys.argv[2]
try:
    ekf_filename = sys.argv[3]
except:
    ekf_filename = None
    ekflines = np.zeros(1)

try:
    stereo_filename = sys.argv[4]
except:
    stereo_filename = None
    stereolines = np.zeros(1)

# Read the files
with open(mono_filename) as f:
    monolines = f.readlines()

with open(gps_filename) as gpsfile:
    gpslines = gpsfile.readlines()

if ekf_filename != None:
    with open(ekf_filename) as ekffile:
        ekflines = ekffile.readlines()

if stereo_filename != None:
    with open(stereo_filename) as stereofile:
        stereolines = stereofile.readlines()

##################### GPS TRAJECTORY #####################

# Initialize arrays to store GPS trajectory
n_gps = len(gpslines) - 1
positions_gt_orig = np.zeros((n_gps, 3))
positions_gt = np.zeros((n_gps, 3))
heading_gt = np.zeros(n_gps)

# Store the GPS positions and heading
for i in range(0, n_gps):
    line = gpslines[i+1].split(',')
    positions_gt_orig[i,0] = line[1] # latitude
    positions_gt_orig[i,1] = line[2] # longitude
    positions_gt_orig[i,2] = line[3] # elevation
    heading_gt[i] = line[4]

# Rotation to account for the heading
theta = np.radians(90 - heading_gt[0])
c, s = np.cos(theta), np.sin(theta)
rotation = np.array([
    [c, -s,  0],
    [s,  c,  0],
    [0,  0,  1]
    ])

# Transform GPS data to meters centered at (0,0)
lat_to_meters = 111139
first_pose = np.array([positions_gt_orig[0,0], positions_gt_orig[0,1], positions_gt_orig[0,2]])
for i in range(0, n_gps):
    # Center at zero
    positions_gt[i] = np.subtract(positions_gt_orig[i], first_pose)
    # Convert to meters
    latitude = positions_gt_orig[i,0]
    lon_to_meters = 111139 * np.cos(np.radians(latitude))
    positions_gt[i,0] = lat_to_meters * positions_gt[i,0]
    positions_gt[i,1] = lon_to_meters * positions_gt[i,1]
    # Rotate to account for heading
    positions_gt[i] = np.matmul(rotation, positions_gt[i])


# Calculate the total distance travelled according to GPS data
dist_gt = 0
dist_gt_2d = 0
for i in range(1, len(positions_gt)):
    diff = np.array([positions_gt[i-1,0] - positions_gt[i,0],
                     positions_gt[i-1,1] - positions_gt[i,1],
                     positions_gt[i-1,2] - positions_gt[i,2]])
    dist_gt += np.linalg.norm(diff)
    dist_gt_2d += np.linalg.norm(diff[0:2])

##################### KEYFRAME TRAJECTORY MONO #####################

# Initialize arrays to store key frame trajectory
n_mono = len(monolines)
timestamps = np.zeros(n_mono)
positions_mono = np.zeros((n_mono, 3))
positions_mono_orig = np.zeros((n_mono, 3))

# Rotation for accounting for the pitch of the cameras
# and starting roll (estimated)
pitch = Rotation.from_euler('x', 90-27.5, degrees=True)
roll  = Rotation.from_euler('y',      -6, degrees=True)
rotation = roll * pitch

# Initialize position and rotation for stitching together multiple maps
quat = Rotation.from_quat(np.array([0,0,0,1]))
map_rotation = Rotation.from_quat(np.array([0,0,0,1]))
map_pose = np.array([0.0,0.0,0.0])

# Store the keyframe trajectory and account for pitch of cameras
for i in range(0, n_mono):
    line_arr = np.asarray(monolines[i].split(), dtype=float)
    timestamps[i] = line_arr[0]
    # Output of ORBSLAM3 is negative for some reason, fix that here
    positions_mono_orig[i] = -1 * line_arr[1:4]

    if i > 1 and np.isclose(positions_mono_orig[i], np.array([0,0,0])).all():
        print("NEW MAP AT INDEX " + str(i))
        # Update stitching parameters
        unit_vec = (positions_mono[i-1] - positions_mono[i-2])/np.linalg.norm(positions_mono[i-1] - positions_mono[i-2])
        y_axis = np.array([0,1,0])
        rotation_axis = np.cross(y_axis, unit_vec)
        rotation_axis = rotation_axis / np.linalg.norm(rotation_axis)
        rotation_angle = np.arccos(np.dot(y_axis, unit_vec))

        map_rotation = Rotation.from_rotvec(rotation_axis * rotation_angle)
        map_pose = np.copy(positions_mono[i-1])
    
    # Update current position
    positions_mono[i] = rotation.apply(positions_mono_orig[i]) #np.matmul(rotation.as_dcm(), positions_mono_orig[i])
    positions_mono[i] = map_rotation.apply(positions_mono[i]) #np.matmul(map_rotation.as_dcm(), positions_mono[i])
    positions_mono[i] += map_pose

# Calculate the total distance travelled according to keyframe trajectory
dist_kf = 0
for i in range(1, len(positions_mono)):
    diff = np.array([positions_mono[i-1,0] - positions_mono[i,0],
                     positions_mono[i-1,1] - positions_mono[i,1],
                     positions_mono[i-1,2] - positions_mono[i,2]])
    dist_kf += np.linalg.norm(diff)

##################### KEYFRAME TRAJECTORY STEREO #####################

# Initialize arrays to store key frame trajectory
n_stereo = len(stereolines)
positions_stereo = np.zeros((n_stereo, 3))
positions_stereo_orig = np.zeros((n_stereo, 3))

# Rotation for accounting for the pitch of the cameras
# and starting roll (estimated)
pitch = Rotation.from_euler('x', 90-27.5, degrees=True)
roll  = Rotation.from_euler('y',      -6, degrees=True)
rotation = roll * pitch

# Initialize position and rotation for stitching together multiple maps
quat = Rotation.from_quat(np.array([0,0,0,1]))
map_rotation = Rotation.from_quat(np.array([0,0,0,1]))
map_pose = np.array([0.0,0.0,0.0])

try:
    # Store the keyframe trajectory and account for pitch of cameras
    for i in range(0, n_stereo):
        line_arr = np.asarray(stereolines[i].split(), dtype=float)
        # Output of ORBSLAM3 is negative for some reason, fix that here
        positions_stereo_orig[i] = -1 * line_arr[1:4]

        if i > 1 and np.isclose(positions_stereo_orig[i], np.array([0,0,0])).all():
            print("NEW MAP AT INDEX " + str(i))
            # Update stitching parameters
            unit_vec = (positions_stereo[i-1] - positions_stereo[i-2])/np.linalg.norm(positions_stereo[i-1] - positions_stereo[i-2])
            y_axis = np.array([0,1,0])
            rotation_axis = np.cross(y_axis, unit_vec)
            rotation_axis = rotation_axis / np.linalg.norm(rotation_axis)
            rotation_angle = np.arccos(np.dot(y_axis, unit_vec))
            # x_axis_sorta = np.cross(y_axis, rotation_axis)
            # x_axis_sorta = x_axis_sorta / np.linalg.norm(x_axis_sorta)
            # x_axis_projection = np.dot(x_axis_sorta, unit_vec)
            # if x_axis_projection < 0:
            #     rotation_angle = 2 * np.pi - rotation_angle
            print(rotation_angle * 180/np.pi)
            map_rotation = Rotation.from_rotvec(rotation_axis * rotation_angle)
            map_pose = np.copy(positions_stereo[i-1])
        
        # Update current position
        positions_stereo[i] = rotation.apply(positions_stereo_orig[i])
        positions_stereo[i] = map_rotation.apply(positions_stereo[i])
        positions_stereo[i] += map_pose
except:
    pass

# Calculate the total distance travelled according to keyframe trajectory
dist_stereo = 0
for i in range(1, n_stereo):
    diff = np.array([positions_stereo[i-1,0] - positions_stereo[i,0],
                     positions_stereo[i-1,1] - positions_stereo[i,1],
                     positions_stereo[i-1,2] - positions_stereo[i,2]])
    dist_stereo += np.linalg.norm(diff)

##################### EKF TRAJECTORY #####################

# Initialize arrays to store EKF trajectory
n_ekf = len(ekflines) - 1
positions_ekf_orig = np.zeros((n_ekf, 3))
positions_ekf = np.zeros((n_ekf, 3))
quaternions_ekf = np.zeros((n_ekf, 4))

try:
    # Store the EKF positions and heading
    for i in range(0, n_ekf):
        line = ekflines[i+1].split(',')
        positions_ekf_orig[i] = line[1:4]
        quaternions_ekf[i] = line[4:8]

    # Transform EKF data to meters centered at (0,0)
    first_pose = np.array([positions_ekf_orig[0,0], positions_ekf_orig[0,1], positions_ekf_orig[0,2]])
    first_quat = quaternions_ekf[0]
    first_quat[0:3] = -1 * quaternions_ekf[0, 0:3]
    rotation = Rotation.from_quat(first_quat).as_dcm()
    print(Rotation.from_quat(first_quat).as_euler('zxy', degrees=True))
    c, s = np.cos(np.pi/2), np.sin(np.pi/2)
    y_forward_rotation = np.array([[c, -s,  0], [s,  c,  0], [0,  0,  1]])
    for i in range(0, n_ekf):
        # Center at zero
        positions_ekf[i] = np.subtract(positions_ekf_orig[i], first_pose)
        # Rotate to account for original quaternion
        positions_ekf[i] = np.matmul(rotation, positions_ekf[i])
        # Rotate to account for y-forward heading
        positions_ekf[i] = np.matmul(y_forward_rotation, positions_ekf[i])
        # Rotate to account for difference in IMU frame?
        positions_ekf[i,0] *= -1
        if np.linalg.norm(positions_ekf[i]) > 250:
            positions_ekf[i] = positions_ekf[i-1]
except:
    pass

# Calculate the total distance travelled according to EKF data
dist_ekf = 0
dist_ekf_2d = 0
for i in range(1, n_ekf):
    diff = np.array([positions_ekf[i-1,0] - positions_ekf[i,0],
                     positions_ekf[i-1,1] - positions_ekf[i,1],
                     positions_ekf[i-1,2] - positions_ekf[i,2]])
    dist_ekf += np.linalg.norm(diff)
    dist_ekf_2d += np.linalg.norm(diff[0:2])

##################### RESCALING AND ERROR CALCULATION #####################

# Account for scale based on GPS data
positions_mono = positions_mono * (dist_gt/dist_kf)

# Calculate the error in positions for keyframe trajectory
error = np.sqrt((positions_gt[-1,0]-positions_mono[-1,0])**2
              + (positions_gt[-1,1]-positions_mono[-1,1])**2
              + (positions_gt[-1,2]-positions_mono[-1,2])**2)

# Calculate the error in positions for EKF trajectory
try:
    error_ekf = np.sqrt((positions_gt[-1,0]-positions_ekf[-1,0])**2
                    + (positions_gt[-1,1]-positions_ekf[-1,1])**2
                    + (positions_gt[-1,2]-positions_ekf[-1,2])**2)
except:
    error_ekf = 0.0
try:
    error_stereo = np.sqrt((positions_gt[-1,0]-positions_stereo[-1,0])**2
                    + (positions_gt[-1,1]-positions_stereo[-1,1])**2
                    + (positions_gt[-1,2]-positions_stereo[-1,2])**2)
except:
    error_stereo = 0.0

# z error
z_error = np.abs(positions_gt[-1,2] - positions_mono[-1,2])
try:
    z_error_ekf = np.abs(positions_gt[-1,2] - positions_ekf[-1,2])
except:
    z_error_ekf = 0.0
try:
    z_error_stereo = np.abs(positions_gt[-1,2] - positions_stereo[-1,2])
except:
    z_error_stereo = 0.0

print('Absolute Error ORBSLAM: {:.5} m'.format(error))
print(' Percent Error ORBSLAM: {:.3%}'.format(error/dist_gt))
print(' Z-drift Error ORBSLAM: {:.5} m').format(z_error)

print('Absolute Error ORBSLAM Stereo: {:.5} m'.format(error_stereo))
print(' Percent Error ORBSLAM Stereo: {:.3%}'.format(error_stereo/dist_gt))
print(' Z-drift Error ORBSLAM Stereo: {:.5} m').format(z_error_stereo)
print('Absolute Distance Error Stereo: {:.5} m'.format(np.abs(dist_stereo - dist_gt)))
print(' Percent Distance Error Stereo: {:.3%}'.format(np.abs(dist_stereo - dist_gt)/dist_gt))

print('Absolute Error EKF: {:.5} m'.format(error_ekf))
print(' Percent Error EKF: {:.3%}'.format(error_ekf/dist_gt))
print(' Z-drift Error EKF: {:.5} m').format(z_error_ekf)
print('Absolute Distance Error EKF: {:.5} m'.format(np.abs(dist_ekf - dist_gt)))
print(' Percent Distance Error EKF: {:.3%}'.format(np.abs(dist_ekf - dist_gt)/dist_gt))
print('Absolute 2D Distance Error EKF: {:.5} m'.format(np.abs(dist_ekf_2d - dist_gt_2d)))
print(' Percent 2D Distance Error EKF: {:.3%}'.format(np.abs(dist_ekf_2d - dist_gt_2d)/dist_gt_2d))

##################### PLOTS #####################

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot3D(positions_gt[:, 0], positions_gt[:, 1], positions_gt[:, 2])
ax.plot3D(positions_mono[:, 0], positions_mono[:, 1], positions_mono[:, 2])
ax.axis('equal')
# ax.set_zlim(0, 80)
ax.legend(['GPS', 'ORB-SLAM3'])
ax.set_title('GPS Data vs. Monocular ORB-SLAM3')
ax.set_xlabel('Left-Right Position (m)')
ax.set_ylabel('Forward Position (m)')
ax.set_zlabel('Elevation (m)')

fig = plt.figure()
ax = plt.axes()
ax.grid()
ax.scatter(positions_gt[:, 0], positions_gt[:, 1], marker='.')
ax.scatter(positions_mono[:, 0], positions_mono[:, 1], marker='.')
ax.axis('equal')
ax.legend(['GPS', 'ORB-SLAM3'])
ax.set_title('GPS Data vs. Monocular ORB-SLAM3')
ax.set_xlabel('Left-Right Position (m)')
ax.set_ylabel('Forward Position (m)')

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot3D(positions_gt[:, 0], positions_gt[:, 1], positions_gt[:, 2])
ax.plot3D(positions_ekf[:, 0], positions_ekf[:, 1], positions_ekf[:, 2])
ax.plot3D(positions_mono[:, 0], positions_mono[:, 1], positions_mono[:, 2])
ax.axis('equal')
# ax.set_zlim(-200, 100)
# ax.legend(['GPS', 'EKF', 'ORB-SLAM3'])
ax.set_title('GPS Data vs. Monocular ORB-SLAM3 vs. Extended Kalman Filter')
ax.set_xlabel('Left-Right Position (m)')
ax.set_ylabel('Forward Position (m)')
ax.set_zlabel('Elevation (m)')

fig = plt.figure()
ax = plt.axes()
ax.grid()
ax.scatter(positions_gt[:, 0], positions_gt[:, 1], marker='.')
ax.scatter(positions_ekf[:, 0], positions_ekf[:, 1], marker='.')
ax.scatter(positions_mono[:, 0], positions_mono[:,1], marker='.')
ax.axis('equal')
ax.legend(['GPS', 'EKF', 'ORB-SLAM3'])
ax.set_title('GPS Data vs. Monocular ORB-SLAM3 vs. Extended Kalman Filter')
ax.set_xlabel('Left-Right Position (m)')
ax.set_ylabel('Forward Position (m)')
# for i in range(len(positions_gt)):
#     plt.annotate(str(i), (positions_gt[i,0], positions_gt[i,1]))

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot3D(positions_gt[:, 0], positions_gt[:, 1], positions_gt[:, 2])
ax.plot3D(positions_mono[:, 0], positions_mono[:, 1], positions_mono[:, 2])
ax.plot3D(positions_stereo[:, 0], positions_stereo[:, 1], positions_stereo[:, 2])
ax.axis('equal')
ax.set_zlim(-50, 50)
# ax.legend(['GPS', 'Monocular', 'Stereo'])
ax.set_title('GPS Data vs. Monocular ORB-SLAM3 vs. Stereo ORB-SLAM3')
ax.set_xlabel('Left-Right Position (m)')
ax.set_ylabel('Forward Position (m)')
ax.set_zlabel('Elevation (m)')

fig = plt.figure()
ax = plt.axes()
ax.grid()
ax.scatter(positions_gt[:, 0], positions_gt[:, 1], marker='.')
ax.scatter(positions_mono[:, 0], positions_mono[:,1], marker='.')
ax.scatter(positions_stereo[:, 0], positions_stereo[:,1], marker='.')
ax.axis('equal')
ax.legend(['GPS', 'Monocular', 'Stereo'])
ax.set_title('GPS Data vs. Monocular ORB-SLAM3 vs. Stereo ORB-SLAM3')
ax.set_xlabel('Left-Right Position (m)')
ax.set_ylabel('Forward Position (m)')

plt.show()