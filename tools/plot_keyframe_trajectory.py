import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

filename = sys.argv[1]

with open(filename) as f:
     lines = f.readlines()

n = len(lines)

timestamps = np.zeros(n)
positions = np.zeros((n, 3))
quaternions = np.zeros((n, 4))

for i in range(0, n):
     line_arr = np.asarray(lines[i].split(), dtype=float)
     timestamps[i] = line_arr[0]
     positions[i] = line_arr[1:4]
     quaternions[i] = line_arr[4:]


fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot3D(positions[:, 0], positions[:, 1], positions[:, 2])
ax.axis('equal')
plt.show()
